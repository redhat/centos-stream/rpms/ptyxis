%global glib2_version 2.78
%global gtk4_version 4.15
%global vte291_version 0.76.3
%global json_glib_version 1.4.0
%global libadwaita_version 1.5.0
%global libportal_gtk4_version 0.7.1

%global tarball_version %%(echo %{version} | tr '~' '.')

Name:		ptyxis
Version:	47.10
Release:	%autorelease
Summary:	A container oriented terminal for GNOME

# Code is GPL-3.0-or-later and the Appdata is CC0-1.0
License:	GPL-3.0-or-later AND CC0-1.0
URL:		https://gitlab.gnome.org/chergert/ptyxis
Source0:	https://download.gnome.org/sources/ptyxis/47/%{name}-%{tarball_version}.tar.xz

BuildRequires:	pkgconfig(gio-unix-2.0) >= %{glib2_version}
BuildRequires:	pkgconfig(gtk4) >= %{gtk4_version}
BuildRequires:	pkgconfig(vte-2.91-gtk4) >= %{vte291_version}
BuildRequires:	pkgconfig(libadwaita-1) >= %{libadwaita_version}
BuildRequires:  pkgconfig(libportal-gtk4) >= %{libportal_gtk4_version}
BuildRequires:	pkgconfig(json-glib-1.0) >= %{json_glib_version}
BuildRequires:	desktop-file-utils
BuildRequires:	gcc
BuildRequires:	itstool
BuildRequires:	meson
BuildRequires:	/usr/bin/appstream-util

Requires:	glib2%{?_isa} >= %{glib2_version}
Requires:	gtk4%{?_isa} >= %{gtk4_version}
Requires:	vte291-gtk4%{?_isa} >= %{vte291_version}
Requires:	json-glib%{?_isa} >= %{json_glib_version}
Requires:	libadwaita%{?_isa} >= %{libadwaita_version}
Requires:	hicolor-icon-theme

Obsoletes:      gnome-terminal <= 3.41
Obsoletes:      gnome-terminal-nautilus <= 3.41

%description
Ptyxis is a container oriented terminal that provides transparent support for
container systems like Podman, Distrobox, and Toolbx. It also has robust
support for user profiles.


%prep
%autosetup -p1 -n %{name}-%{tarball_version}


%build
%meson -Dgeneric=terminal
%meson_build


%install
%meson_install

%find_lang %{name} --with-gnome


%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/org.gnome.Ptyxis.metainfo.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Ptyxis.desktop


%files -f %{name}.lang
%doc README.md NEWS
%license COPYING
%{_bindir}/ptyxis
%{_libexecdir}/ptyxis-agent
%{_metainfodir}/org.gnome.Ptyxis.metainfo.xml
%{_datadir}/applications/org.gnome.Ptyxis.desktop
%dir %{_datadir}/dbus-1
%dir %{_datadir}/dbus-1/services
%{_datadir}/dbus-1/services/org.gnome.Ptyxis.service
%{_datadir}/glib-2.0/schemas/org.gnome.Ptyxis.gschema.xml
%{_datadir}/icons/hicolor/*/*/*.svg
%{_mandir}/man1/ptyxis.1*


%changelog
%autochangelog
